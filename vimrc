execute pathogen#infect()
syntax on
filetype plugin indent on
set t_Co=256
set background=dark
set tabstop=4
set shiftwidth=4
set softtabstop=4
set expandtab
set encoding=utf-8
set scrolloff=10
set showmode
set showcmd
set hidden
set wildmenu
set wildmode=longest,list,full
set ttyfast
set ruler
set laststatus=2
set number
set relativenumber
set ignorecase
set smartcase
set incsearch
set showmatch
set wrap
set linebreak
set nolist
set colorcolumn=80
set cindent
set smarttab
set modelines=5
set backspace=indent,eol,start
